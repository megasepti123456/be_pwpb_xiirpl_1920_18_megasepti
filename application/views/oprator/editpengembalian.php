<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>edit pengembalian</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/detailbarang.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
				<form action="" method="post">
					<h3>Edit Pengembalian</h3>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="">Id pengembalian</label>
							<input type="text" class="form-control" placeholder="001">
						</div>
						<div class="form-wrapper">
							<label for="">tanggal pinjam</label>
							<input type="text" class="form-control" placeholder="09-08-2019">
						</div>
					</div>
					<div class="form-wrapper">
						<label for="">tanggal kembali</label>
						<input type="text" class="form-control" placeholder="20-08-2019">
					</div>
					
					
					<button>Ubah</button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>