<!DOCTYPE html>
<html>
<head>
     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>DATA inventaris</title>
    <meta charset="utf-8"/>
    
    <style>
        body{
            /* background-image: url(<?=base_url();?>assets/img/ty.jpg); */
        }
        header
{
    position: relative;
    z-index: 1000000;
}
p
{
    font-size: 1.15rem
}
.navbar.scrolled
{
    background: #fff;
}
.navbar
{
    background: transparent!important;
    transition: 0.5s;

}
.navbar.scrolled
{
    background: #fff !important;
    transition: 0.5s;
}
.navbar-light .navbar-nav .active>.nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show>.nav-link {
    color: #fff;
    background: #000;
}
.navbar-light .navbar-nav .nav-link {
    color:#000;
}
.navbar-light .navbar-nav .nav-link:hover,
.navbar-light .navbar-nav .nav-link:focus
{
    color: rgba(255,255,255,.1);
}
.navbar-light .navbar-brand, 
.navbar-light .navbar-brand:hover, 
.navbar-light .navbar-brand:focus,
.navbar-light .navbar-brand:visited 
{
    color: blue;
    font-size: 1.5rem;
    font-weight: 600;
   
}
        .kotak{
            margin: 20px auto;
            max-width: 960px;
        }
        .table{
            width: 100%;
            max-width: 100%;
            margin-bottom: 2rem;
            background-color: white;
            margin-top:50px;
        }
        .table > thead > tr,
        .table > tbody > tr,
        .table > tfoot > tr {
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
            text-align: left;
            padding: 1.6rem;
            vertical-align: top;
            border-top: 0;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        .table > thead > tr > th {
            font-weight: 400;
            color: #000;
            vertical-align: bottom;
            border-bottom: 1px solid rgba(0,0,0,0.12);
        }
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > th,
        .table > caption + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > td,
        .table > thead:first-child > tr:first-child > td {
            border-top: 0;
        }
        .table > tbody + tbody {
            border-top: 1px solid rgba(0,0,0,0.12);
        }
        .table .table{
            background: #fff;
        }
        .table .no-border{
            border: 0;
        }
        .table-condensed > thead > tr > th,
        .table-condensed > tbody > tr > th,
        .table-condensed > tfoot > tr > th,
        .table-condensed > thead > tr > td,
        .table-condensed > tbody > tr > td,
        .table-condensed > tfoot > tr > td {
            padding: 0.8rem;
        }
        .table-bordered{
            border: 0;
        }
        .table-bordered > thead > tr > th,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > tbody > tr > td,
        .table-bordered > tfoot > tr > td {
            border: 0;
            border-bottom: 1px solid rgba(200, 212, 249, 0.9);
        }
        .table-bordered > thead > tr > th,
        .table-bordered > thead > tr > td {
            border-bottom-width: 2px;
        }
        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th{
            background-color: rgba(200, 212, 249, 0.9);;
        }
        .table-hover > tbody > tr:hover > td,
        .table-hover > tbody > tr:hover > th {
            background-color: rgba(0,0,0,0.12);
        }
        @media screen and (max-width: 768px){
            .table-responsive-vertical > .table{
                margin-bottom: 0;
                background-color: transparent;
            }
            .table-responsive-vertical > .table > thead,
            .table-responsive-vertical > .table > tfoot {
                display: none;
            }
            .table-responsive-vertical > .table > tbody{
                display: block;
            }
            .table-responsive-vertical > .table > tbody > tr{
                display: block;
                border: 1px solid #e0e0e0;
                border-radius: 2px;
                margin-bottom: 1.6rem;
            }
            .table-responsive-vertical > .table > tbody > tr > td {
                background-color: #fff;
                display: block;
                vertical-align: middle;
                text-align: right;
            }
            .table-responsive-vertical > .table > tbody > tr > td[data-title]:before{
                content: attr(data-title);
                float: left;
                font-size: inherit;
                font-weight: 400;
                color: #757575;
            }
            .table-responsive-vertical > .table-bordered {
                border: 0;
            }
            .table-responsive-vertical > .table-bordered > tbody > tr > td{
                border: 0;
                border-bottom: 1px solid #e0e0e0;
            }
            .table-responsive-vertical > .table-bordered > tbody > tr > td:last-child{
                border-bottom: 0;
            }
            .table-responsive-vertical > .table-bordered > tbody > tr > td,
            .table-responsive-vertical > .table-bordered > tbody > tr:nth-child(odd){
                background-color: #fff;
            }
            .table-responsive-vertical > .table-bordered > tbody > tr > td:nth-child(odd){
                background-color: rgba(200, 212, 249, 0.9);
            }
            .table-responsive-vertical > .table-hover > tbody > tr:hover > td,
            .table-responsive-vertical > .table-hover > tbody > tr:hover {
                background-color: #fff;
            }
            .table-responsive-vertical > .table-hover > tbody > tr > td:hover{
                background-color: rgba(0,0,0,0.12);
            }
        }
        .table-striped.table-mc-red > tbody > tr:nth-child(odd) > td,
        .table-striped.table-mc-red > tbody > tr:nth-child(odd) > th {
            background-color:rgba(200, 212, 249, 0.9);
        }
        .table-hover.table-mc-red > tbody > tr:hover > td,
        .table-hover.table-mc-red > tbody > tr:hover > th {
            background-color: rgba(200, 212, 249, 0.9);
        }

        h1{
            font-size : 20pt;
        }
        </style>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top scrolled">
            <div class="container">
                <a class="navbar-brand" href="#">Inventory</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#exsplore">Exsplore</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
   
  </div> 
<br><br><br>
<div class="kotak">
        <h1 style="text-align:center; color: #000!important;">DATA PENGEMBALIAN</h1>
        <div class="row  mt-3">
            <div class="col-md-6">
               <a href="<?=base_url();?>pengembalianop/tambah" class="btn btn-primary">Tambah Data Pengembalian</a>
            </div>
        </div>

        </div> <form class="form-inline my-2 my-lg-0" style="float:right; ">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<center>
        <div class="table-responsive-vertical" style ="width : 80%; ">
            <table class="table table-bordered table-striped table-hover table-mc-red"style="">
                <thead>
                    <tr> 
                       <th>No</th>
                        <th>id_pengembalian</th>
                        <th>tanggal_kembali</th>
                        <th>id_peminjam</th>
                        <th>id_peminjaman</th>
                        <th>id_inventaris</th>
                        <th>kondisi</th>
                    </tr>
                </thead> 
                <tbody>
                </td>
                <tr> 
                    <?php $i=1;
                     foreach ( $pmblanop as $pmbl) : ?>
                        <td data-title="no"><?= $i;?></td> 
                        <td data-title="id_pengembalian"><?=$pmbl['id_pengembalian'];?></td>
                        <td data-title="tanggal_kembali"><?=$pmbl['tanggal_kembali'];?></td>
                        <td data-title="id_peminjam"><?=$pmbl['id_peminjam'];?></td>
                        <td data-title="id_peminjaman"><?=$pmbl['id_peminjaman'];?></td>
                        <td data-title="id_inventaris"><?=$pmbl['id_inventaris'];?></td>
                        
                        <td><a href="<?=base_url();?>pengembalianop/ubah/<?=$pmbl['id_pengembalian'];?>" class="btn btn-info">edit</a>
                       
                        <a href="<?=base_url('pengembalianop/hapus') . '/' .$pmbl['id_pengembalian'] ?>" class="btn btn-danger">hapus</a></td> </tr>
                        <?php $i++;
                        endforeach ;?>
                 
                </tbody>
            </table>
            <a href="<?=base_url();?>hpetugas">kembali</a>
        </div>
    </div>
        

</body>
</html>