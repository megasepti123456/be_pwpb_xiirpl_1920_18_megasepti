<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ubah data pengembalian</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
                
                
				<!-- <form action="<?=base_url('pengembalianop/ubah/');?>" method="post"> -->
				<form action="" method="post">
                    <h3>ubah data pengembalian</h3>
                    <br>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_pengembalian">Id Pengembalian</label>
                            <input type="text" class="form-control" id="id_pengembalian" name="id_pengembalian"value="<?=$pmlop->id_pengembalian;?>">
                            <?= form_error('id_pengembalian','<small class="text-denger pl-3">','</small>')?>
						</div>
                        <div class="form-wrapper">
							<label for="tanggal_kembali">tanggal_kembali</label>
                            <input type="date" class="form-control" id="tanggal_kembali" name="tanggal_kembali"value="<?=$pmlop->tanggal_kembali;?>">
                            <?= form_error('tanggal_kembali','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_peminjam">id_peminjam</label>
                            <input type="text" class="form-control" id="id_peminjam" name="id_peminjam"value="<?=$pmlop->id_peminjam;?>">
                          
						</div>
						<div class="form-wrapper">
							<label for="id_peminjaman">id_peminjaman</label>
                            <input type="text" class="form-control" id="id_peminjaman" name="id_peminjaman"value="<?=$pmlop->id_peminjaman;?>">
                          
						</div>
						<div class="form-wrapper">
							<label for="id_inventaris">id_inventaris</label>
                            <input type="text" class="form-control" id="id_inventaris" name="id_inventaris"value="<?=$pmlop->id_inventaris;?>">
                          
						</div>
						
					
					<button type="submit" name="ubah" class="btn btn primary"style="color: #fff!important;margin-top:20%;">Ubah</a></button>
				</form>
			</div>
		</div> 
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>