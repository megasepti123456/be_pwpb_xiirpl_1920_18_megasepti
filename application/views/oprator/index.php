<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta charset="utf-8">
	<title>HOMEPAGE OPRATOR</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/hpadmin.css">


</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top scrolled">
			<div class="container">
				<a class="navbar-brand" href="#">Inventory</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#exsplore">Exsplore</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#about">About</a>
						</li>
						<li class="nav-link">
		<?php echo $this->session->userdata('ses_nama');?></h2><a href="<?php echo base_url().'hpetugas/logout'?>">Sign Out</a>
		</div>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<div class="image-top" id="home">
        <div class="lapisanimage">
          <center><h1 class="txt-it1">SELAMAT DATANG </h1><br>
            <h3 class="txt-it2">OPERATOR</h3>
              
              </center>
            </div>
          </div>
	<section class="sec1" id="exsplore">
		<div class="container">
			<div class="row">
				<div class="offset-sm-2 col-sm-8">
					<div class="headerText text-center">
						<h2>Explore data</h2>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">

					</div>
					<div class="content">
						<h3>Peminjaman <br> <span><a href="<?= base_url(); ?>peminjamanop/index">data peminjaman</a></span></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>Petugas<br> <span><a href="<?= base_url(); ?>Petugasd">Data Petugas</span></a></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>Pengembalian<br> <span><a href="<?= base_url(); ?>Pengembalianop">Data Pengembalian</a></span></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="sec2" id="about">
	<div class="offset-sm-2 col-sm-8">
		<div class="container h100">
			<div class="contenBox h100">
				<div class="headerText text-center">
					<h2>Inventory</h2>
					<p>Dengan menggunakan program aplikasi Inventory Barang, semua alat / aset barang yang ada di sekolah dapat didata dengan baik, jumlah dan keberadaannya dapat diketahui dengan mudah.</p>
					
					<a href="#" class="btn btnD1">Read More</a>
				</div>
			</div>
		</div>
	</div>
</section>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(document).scroll(function(){
		$('.navbar').toggleClass('scrolled', $(this).
			scrollTop() > $('.navbar').height());
	});
</script>


</body>
</html>