<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ubah Data Petugas</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
                
                
				<form action="<?= base_url('petugasd/ubahpetugasop');?>" method="post">
               
                    <h3>ubah data petugas</h3>
                    <br>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_petugas">Id petugas</label>
                            <input type="text" class="form-control" id="id_petugas" name="id_petugas" value="<?= $ptgop->id_petugas;?>">
                            <?= form_error('id_petugas','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="username">username</label>
                            <input type="text" class="form-control" id="username" name="username" value="<?= $ptgop->username;?>">
                            <?= form_error('username','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="password">password</label>
                            <input type="text" class="form-control" id="password" name="password" value="<?= $ptgop->password;?>">
                            <?= form_error('password','<small class="text-denger pl-3">','</small>')?>
                        </div>
                       
					</div>
					
					 
					<button type="submit" name="ubah" class="btn btn primary"style="color: #fff!important;">Ubah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>