<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Tambah Data peminjaman</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/ee.jpg');">
			<div class="inner">
                
                
				<form action="<?=base_url('detailpj/tambah');?>" method="POST" enctype="multipart/form-data">
                    <h3>Tambah data peminjam</h3>
                    <br>
					
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_inventaris">id inventaris</label>
                            <input type="text" class="form-control" id="id_inventaris" name="id_inventaris">
                            <?= form_error('id_inventaris','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_peminjaman">Id peminjaman</label>
                            <input type="text" class="form-control" id="id_peminjaman" name="id_peminjaman" >
                            <?= form_error('id_peminjaman','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_detail_pinjam">Id detail pinjam</label>
                            <input type="text" class="form-control" id="id_detail_pinjam" name="id_detail_pinjam">
                            <?= form_error('id_detail_pinjam','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="jumlah">Jumlah</label>
                            <input type="text" class="form-control" id="jumlah" name="jumlah">
                            <?= form_error('jumlah','<small class="text-denger pl-3">','</small>')?>
						</div>
						
					
					<button type="submit" name="tambah" class="btn btn primary"style="color: #fff!important;">Tambah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>