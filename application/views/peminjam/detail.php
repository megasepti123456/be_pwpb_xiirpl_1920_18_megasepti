<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-6">
                <div class="card">
                <div class="card-header">
                    Detail Data peminjam 
                </div>
                <div class="card-body">
                    <h5 class="card-title">id detail pinjam: <?=$detail_pinjam->id_detail_pinjam;?></h5>
                    <p class="card-text">tanggal pinjam :<?=$detail_pinjam->tanggal_pinjam;?></p>
                    <p class="card-text">tanggal kembali:<?=$detail_pinjam->tanggal_kembali; ?></p>
                    <p class="card-text">jumlah:<?=$detail_pinjam->jumlah; ?></p>
                    
                                        
                    
                    <a href="<?= base_url('detailpj'); ?>" class="btn btn-primary">Kembali</a>
                </div>
                </div>
            </div>
           
        </div>
    </div> 
    
</body>
</html>