<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>DATA inventaris</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/listbarang.css">
    <meta charset="utf-8"/>
    


</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top scrolled">
			<div class="container">
				<a class="navbar-brand" href="#">Inventory</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#exsplore">Exsplore</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#about">About</a>
						</li>
						<li class="nav-link">
		<?php echo $this->session->userdata('ses_nama');?></h2><a href="<?php echo base_url().'hpeminjam/logout'?>">Sign Out</a>
		</div>
					</ul>
				</div>
			</div>
		</nav>
	</header>

<div class="container d-flex justify-content-center p-0">
	<div class ="col-md-12 d-flex m-5 justify-content-center p-0 pt-5" style=" flex-flow: row wrap">
		<?php
		foreach($inventaris as $in)
		{ ?>
		<div class="m-3 p-3 bg-white" style="max-width: 250px;">
			<img src="<?=base_url('uploads').'/'.$in['gambar'];?>" class="card-img-top" style=" min-height: 200px !important; max-height: 200px !important" alt="...">
			<center> <p class="card-text"><?=$in['nama'];?></a></p><center>
			<div class="col-md-12 d-flex justify-content-center">
				<a href="<?=base_url();?>detailpj/tambah" class="btn btn-primary">pinjam</a>
				<a href="<?=base_url('peminjamanpj/detail/' . $in['id_inventaris']);?>" class="btn btn-primary" style="margin-left:50px;">detail</a>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

	
        <!-- <section> -->
        	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script type="text/javascript">
		$(document).scroll(function(){
			$('.navbar').toggleClass('scrolled', $(this).
				scrollTop() > $('.navbar').height());
	});
        // </section>
        </body>

</html>