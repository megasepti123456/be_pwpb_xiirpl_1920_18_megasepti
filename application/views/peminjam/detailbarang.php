<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-6">
                <div class="card">
                <div class="card-header">
                    Detail Data inventaris
                </div>
                <div class="card-body">
                    <h5 class="card-title">id inventaris : <?=$invers->id_ruang;?></h5>
                    <p class="card-text">id jenis :<?=$invers->id_jenis;?></p>
                    <p class="card-text">id ruang :<?=$invers->id_ruang; ?></p>
                    <p class="card-text">nama :<?=$invers->nama; ?></p>
                    <p class="card-text">keterangan :<?=$invers->keterangan; ?></p>
                    <p class="card-text">jumlah :<?=$invers->jumlah; ?></p>
                    <p class="card-text">kode inventaris :<?=$invers->kode_inventaris; ?></p>
                    <h5 class="card-tittle"><img src="<?= base_url().'/'.$invers->gambar; ?>" width="100;" alt=""></h5>
                    <a href="<?= base_url('Listbarang'); ?>" class="btn btn-primary">Kembali</a>
                </div>
                </div>
            </div>
        </div>
    </div> 
</body>
</html>