<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Tambah Data Peminjaman</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
                
                
				<form action="<?=base_url();?>peminjamanpj/tambah" method="post">
                    <h3>Tambah data peminjaman</h3>
                    <br>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_peminjam">Id Peminjam</label>
                            <input type="text" class="form-control" id="id_peminjam" name="id_peminjam">
                            <?= form_error('id_peminjam','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_peminjaman">id peminjaman</label>
                            <input type="text" class="form-control" id="id_peminjaman" name="id_peminjaman">
                            <?= form_error('id_peminjaman','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="tanggal_pinjam">tanggal_pinjam</label>
                            <input type="date" class="form-control" id="tanggal_pinjam" name="tanggal_pinjam">
                            <?= form_error('tanggal_pinjam','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="tanggal_kembali">tanggal kembali</label>
                            <input type="date" class="form-control" id="tanggal_kembali" name="tanggal_kembali">
                            <?= form_error('tanggal_kembali','<small class="text-denger pl-3">','</small>')?>
						</div>
					</div>
					<div class="form-wrapper">
						<label for="status_peminjaman">status_peminjaman</label>
                        <input type="text" class="form-control" id="status_peminjaman" name="status_peminjaman">
                        <?= form_error('status_peminjaman','<small class="text-denger pl-3">','</small>')?>
					</div>
					
                      
					
					
					<button type="submit" name="tambah" class="btn btn primary"style="color: #fff!important;">Tambah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>