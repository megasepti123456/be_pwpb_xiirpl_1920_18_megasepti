<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>ubah Data Peminjam</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
                
                
				<form action="" method="post">
               
                    <h3>ubah data peminjam</h3>
                    <br>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_peminjam">Id Peminjam</label>
                            <input type="text" class="form-control" id="id_peminjam" name="id_peminjam" value="<?= $pj->id_peminjam;?>">
                            <?= form_error('id_peminjam','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-group">
						<div class="form-wrapper">
							<label for="id_peminjam">Id level</label>
                            <input type="text" class="form-control" id="id_level" name="id_level" value="<?= $pj->id_level;?>">
                            <?= form_error('id_level','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" value="<?= $pj->username;?>">
                            <?= form_error('username','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="password">password</label>
                            <input type="text" class="form-control" id="password" name="password" value="<?= $pj->password;?>">
                            <?= form_error('password','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="nama_peminjam">Nama Peminjam</label>
                            <input type="text" class="form-control" id="nama_peminjam" name="nama_peminjam" value="<?= $pj->nama_peminjam;?>">
                            <?= form_error('nama_peminjam','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="alamat">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" value="<?= $pj->alamat;?>">
                            <?= form_error('alamat','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        
					 
					<button type="submit" name="ubah" class="btn btn primary"style="color: #fff!important;">Ubah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>