<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Tambah Data Peminjaman</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/rere.jpg');">
			<div class="inner">
                
                
				<form action="" method="post">
                    <h3>Tambah data ruangan</h3>
                    <br>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_ruang">Id Ruang</label>
                            <input type="text" class="form-control" id="id_ruang" name="id_ruang">
                            <?= form_error('id_ruang','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="nama_ruang">nama ruang</label>
                            <input type="text" class="form-control" id="nama_ruang" name="nama_ruang">
                            <?= form_error('nama_ruang','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="kode_ruang">kode_ruang</label>
                            <input type="text" class="form-control" id="kode_ruang" name="kode_ruang">
                            <?= form_error('kode_ruang','<small class="text-denger pl-3">','</small>')?>
                        </div>
                        <div class="form-wrapper">
							<label for="keterangan">keterangan</label>
                            <input type="text" class="form-control" id="keteragan" name="keterangan">
                            <?= form_error('keterangan','<small class="text-denger pl-3">','</small>')?>
                        </div>
					
					<button type="submit" name="tambah" class="btn btn primary"style="color: #fff!important;margin-top:20%;">Tambah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>