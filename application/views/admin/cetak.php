<!DOCTYPE html>
<html lang="en">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <style>
    .table1 {
    font-family: sans-serif;
    color: #232323;
    border-collapse: collapse;
    }
 
    .table1, th, td {
    border: 1px solid #999;
    padding: 8px 20px;
}
    </style>
</head>
<body>
<div class="kotak">
        <h1 style="text-align:center; color: #000!important;">DATA INVENTARIS</h1>
  </div>
</nav>
<center>
        <div class="table1" style ="width : 80%; ">
            <table class="table table-bordered table-striped table-hover table-mc-red">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>gambar</th>
                        <th>id_inventaris</th>
                        <th>nama</th>
                        <th>keterangan</th>
                        <th>jumlah</th>
                        <th>kode inventaris</th>
                       
                        

                    </tr>
                </thead> 
                <tbody>
                <tr>

                <?php $i=1;
                     foreach ( $inventaris as $in) : ?>
                        <td data-title="no"><?= $i;?></td> 
                        <td><img src="<?= base_url('uploads/').$in['gambar']; ?>" width="100" class=""></td><br>
                        <td data-title="id_inventaris"><?=$in['id_inventaris'];?></td><br>
                        <td data-title="nama"><?=$in['nama'];?></td><br>
                        <td data-title="keterangan"><?=$in['keterangan'];?></td>
                        <td data-title="jumlah"><?=$in['jumlah'];?></td>
                        <td data-title="kode_inventaris"><?=$in['kode_inventaris'];?></td>
                        </tr>
                   
                    <?php $i++;
                        endforeach ;?>
                        
                   
                   </tr>
                </tbody>
            </table>
            <script>
                window.print();
            </script>
</body>
</html>