<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta charset="utf-8">
	<title>HOMEPAGE ADMIN</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/hpadmin.css">
	 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top scrolled">
			<div class="container">
				<a class="navbar-brand" href="#">Inventory</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#exsplore">Exsplore</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#about">About</a>
						</li>
						
						<li class="nav-link">
		<?php echo $this->session->userdata('ses_nama');?></h2><a href="<?php echo base_url().'hpadmin/logout'?>">Sign Out</a>
		</div>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<div class="image-top" id="home">
        <div class="lapisanimage">
          <center><h1 class="txt-it1">SELAMAT DATANG </h1><br>
            <h3 class="txt-it2">ADMIN</h3>
              
              </center>
            </div>
          </div>
	<section class="sec1" id="exsplore">
		<div class="container">
			<div class="row">
				<div class="offset-sm-2 col-sm-8">
					<div class="headerText text-center">
						<h2>Explore data</h2>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">

					</div>
					<div class="content">
						<h3>Peminjaman <br> <span><a href="<?=base_url();?>Peminjamand">data peminjman</a></span></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>Inventaris<br> <span><a href="<?=base_url();?>inventarisd">Data Inventaris</span></a></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>Pengembalian<br> <span><a href="<?=base_url();?>pengembaliand">Data Pengembalian</a></span></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">

					</div>
					<div class="content">
						<h3>ruangan <br> <span><a href="<?=base_url();?>Ruangan">data ruangan</a></span></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>petugas<br> <span><a href="<?=base_url();?>petugas">data petugas</span></a></h3>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="placeBox">
					<div class="imgBx">
						<img src="<?= base_url(); ?>assets/img/peminjaman.png" class="img-fluid">
					</div>
					<div class="content text-center">
						<h3>peminjam<br> <span><a href="<?=base_url();?>peminjam">Data Peminjam</a></span></h3>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<section class="sec2" id="about">
	<div class="offset-sm-2 col-sm-8">
		<div class="container h100">
			<div class="contenBox h100">
				<div class="headerText text-center">
					<h2>Inventory</h2>
					<p>Dengan menggunakan program aplikasi Inventory Barang, semua alat / aset barang yang ada di sekolah dapat didata dengan baik, jumlah dan keberadaannya dapat diketahui dengan mudah.</p>
					
					<a href="#" class="btn btnD1">Read More</a>
				</div>
			</div>
		</div>
	</div>


 <footer>
    <div class="konten1">
        <div class="line"></div>
            <h3>inventory</h3>
            
            <P>Dengan menggunakan program aplikasi Inventory Barang, semua alat / aset barang yang ada di sekolah dapat didata dengan baik, jumlah dan keberadaannya dapat diketahui dengan mudah.
            </P>
        </div>
        <div class="konten2">
            <h5>OUR TEAM</h5>
                <div class="line2">
                    <div class="l-line"></div>
                </div>
                <li><a href="#">petugas</a></li><hr class="line2">
                <li><a href="#">peminjam</a></li><hr class="line2">
                <li><a href="#">admin</a></li><hr class="line2">
               
        </div>
        <div class="konten3">
            <h5>FOLLOW US</h5>
                <div class="line2">
                    <div class="l-line"></div>
                    <li><a href="#"><i class="fab fa-twitter t" style="margin-left: -60px; padding: 10px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-facebook f" style="margin-left: -5px; padding: 10px 12px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-instagram i" style="margin-left: 10px; padding: 10px 12px; border-radius: 100%;"></i></a></li>
                    <li><a href="#"><i class="fab fa-youtube y" style="margin-left: 10px; padding: 10px 10px; border-radius: 100%;"></i></a></li>
                </div>
        </div>
        <div class="konten4">
            <h5>OUR NEWSLETTER</h5>
                <div class="line2">
                    <div class="l-line"></div>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum totam pariatur dolor atque minus, laborum impedit eligendi vitae recusandae, distinctio neque itaque, fugit magni incidunt harum aliquid tenetur deleniti! Vero?</p> 
        </div>
    <div class="footer-bottom">
        <p>Copyright &copy; 2019 Megasepti Production</p>
    </div>
    </footer>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">
	$(document).scroll(function(){
		$('.navbar').toggleClass('scrolled', $(this).
			scrollTop() > $('.navbar').height());
	});
</script>


</body>
</html>