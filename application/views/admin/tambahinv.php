<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Tambah Data Inventaris</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
	</head>

	<body>

		<div class="wrapper" style="background-image: url('<?=base_url();?>assets/img/ee.jpg');">
			<div class="inner">
                
                
				<form action="<?=base_url('inventarisd/tambah');?>" method="POST" enctype="multipart/form-data">
                    <h3>Tambah data Inventaris</h3>
                    <br>
					<div class="form-wrapper">
							<label for="gambar">gambar</label>
							<input type="file" name="gambar" id="gambar">
                            <?= form_error('gambar','<small class="text-denger pl-3">','</small>')?>
						</div>
					<div class="form-group">
						<div class="form-wrapper">
							<label for="id_inventaris">id inventaris</label>
                            <input type="text" class="form-control" id="id_inventaris" name="id_inventaris">
                            <?= form_error('id_inventaris','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_jenis">Id jenis</label>
                            <input type="text" class="form-control" id="id_jenis" name="id_jenis" >
                            <?= form_error('id_jenis','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="id_ruang">Id ruang</label>
                            <input type="text" class="form-control" id="id_ruang" name="id_ruang">
                            <?= form_error('id_ruang','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                            <?= form_error('nama','<small class="text-denger pl-3">','</small>')?>
						</div>
						
						<div class="form-wrapper">
							<label for="kondisi">Kondisi</label>
                            <input type="text" class="form-control" id="kondisi" name="kondisi">
                            <?= form_error('kondisi','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="keterangan">Keterangan</label>
                            <input type="text" class="form-control" id="keterangan" name="keterangan">
                            <?= form_error('keterangan','<small class="text-denger pl-3">','</small>')?>
						</div>
						
						<div class="form-wrapper">
							<label for="jumlah">Jumlah</label>
                            <input type="text" class="form-control" id="jumlah" name="jumlah">
                            <?= form_error('jumlah','<small class="text-denger pl-3">','</small>')?>
						</div>
						<div class="form-wrapper">
							<label for="kode_inventaris">kode inventaris</label>
                            <input type="text" class="form-control" id="kode_inventaris" name="kode_inventaris">
                            <?= form_error('kode_inventaris','<small class="text-denger pl-3">','</small>')?>
						</div>
						
					
					<button type="submit" name="tambah" class="btn btn primary"style="color: #fff!important;">Tambah</a></button>
				</form>
			</div>
		</div>
		
	</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>