<?php


class Room_op_pmjm extends Ci_model{
    public function getAllPmjmop()
    {
        return $this->db->get('peminjaman')->result_array();
     
    }
    public function tambahDataPeminjamanop()
    {
        $data = [
            "id_peminjam" => $this->input->post('id_peminjam', true),
            "id_peminjaman" => $this->input->post('id_peminjaman',true),
            "tanggal_pinjam" => $this->input->post('tanggal_pinjam',true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali',true),
            "status_peminjaman" => $this->input->post('status_peminjaman',true)
        
        ];
        //var_dump ($data); die;  

        $this->db->insert('peminjaman',$data);
    }
    public function cariDataoppeminjaman() {
    	$keyword = $this->input->get('keyword', true);
    	$this->db->like('nama', $keyword);
    	
    	return $this->db->get('peminjaman')->result_array(); 
    }
    public function hapusDataPeminjamanop($where)
    {
       
        $this->db->delete('peminjaman',$where);
    }
    public function getPeminjamanopById($id)
    {
        return $this->db->get_where('peminjaman',['id_peminjaman' => $id])->row();
    }
    public function ubahDataPeminjamanop()
    {
        $data = [
            "id_peminjam" => $this->input->post('id_peminjam', true),
            "id_peminjaman" => $this->input->post('id_peminjaman',true),
            "tanggal_pinjam" => $this->input->post('tanggal_pinjam',true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali',true),
            "status_peminjaman" => $this->input->post('status_peminjaman',true)
        
        ];
        //var_dump ($data); die;
        $this->db->where('id_peminjaman', $this->input->post('id_peminjaman'));
        $this->db->update('peminjaman',$data);
    }
} 
?> 
  