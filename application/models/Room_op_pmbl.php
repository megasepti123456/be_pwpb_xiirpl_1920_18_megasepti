<?php


class Room_op_pmbl extends Ci_model{
    public function getAllPmblop() 
    {
        return $this->db->get('pengembalian')->result_array();
     
    }
    public function cariDatapmblop() {
    	$keyword = $this->input->get('keyword', true);
    	$this->db->like('id_pengembalian', $keyword);
    	
    	return $this->db->get('pengembalian')->result_array(); 
    } 
    public function tambahDataPengembalianop()
    {
        $data = [
            "id_pengembalian" => $this->input->post('id_pengembalian', true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali', true),
            "id_peminjam" => $this->input->post('id_peminjam', true),  
            "id_peminjaman" => $this->input->post('id_peminjaman', true),    
            "id_inventaris" => $this->input->post('id_inventaris', true)         
          
         
        ];
        // var_dump ($data); die;
 
        $this->db->insert('pengembalian',$data);
    }
    public function hapusDataPengembalianop($where)
    {
      
        $this->db->delete('pengembalian',$where);
    } 
    public function getPengembalianopById($id)
    {
        return $this->db->get_where('pengembalian',['id_pengembalian' => $id])->row();  
    }
    public function ubahDataPengembalian()
    {
        $data = [
            "id_pengembalian" => $this->input->post('id_pengembalian', true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali', true),
            "id_peminjam" => $this->input->post('id_peminjam', true),
            "id_peminjaman" => $this->input->post('id_peminjaman', true),
            "id_inventaris" => $this->input->post('id_inventaris', true)
          
        
        ];
        // var_dump ($data); die;
        $this->db->where('id_pengembalian', $this->input->post('id_pengembalian'));
        $this->db->update('pengembalian',$data);
    } 
}
?>
