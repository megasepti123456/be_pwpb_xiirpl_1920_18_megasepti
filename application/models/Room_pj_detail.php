<?php 


class Room_pj_detail extends Ci_model{
    public function getAllpj()
    {
        return $this->db->get('ppeminjaman')->result_array();
     
    }

    public function getRoom_pj_detailById($id)
	{
		return $this->db->get_where('detail_pinjam', ['id_detail_pinjam' => $id])->row_array();
	}
   
    public function tambahDataPeminjam(){
        $data = [
            "id_detail_pinjam" => $this->input->post('id_detail_pinjam', true),
            "id_peminjaman" => $this->input->post('id_peminjaman', true),
            "id_inventaris" => $this->input->post('id_inventaris', true),
            "jumlah" => $this->input->post('jumlah',true),
            
            
        
        ];
        
        $this->db->insert('detail_pinjam',$data);
    }

    public function getPeminjamanpjById($id)
    {
        return $this->db->get_where('ppeminjaman',['id_detail_pinjam' => $id])->row();  
    }

       
    // public function hapusDataInventaris($where)
    // {
      
    //     $this->db->delete('inventaris',$where);
    // }
    // public function getInventarisById($id)
    // {
    //     return $this->db->get_where('inventaris',['id_inventaris' => $id])->row();
    // }
    // public function ubahDataInventaris()
    // {
       
    //     $data = [
          
    //         "id_inventaris" => $this->input->post('id_inventaris', true),
    //         "id_jenis" => $this->input->post('id_jenis', true),
    //         "id_ruang" => $this->input->post('id_ruang', true),
    //         "nama" => $this->input->post('nama',true),
    //         "gambar" => $_FILES['gambar']['name'],
    //         "kondisi" => $this->input->post('kondisi',true),
    //         "keterangan" => $this->input->post('keterangan',true),
    //         "jumlah" => $this->input->post('jumlah',true),
    //         "kode_inventaris" => $this->input->post('kode_inventaris',true)
        
    //     ];
       
        
    //     // var_dump ($data); die;
    //     $this->db->where('id_inventaris', $this->input->post('id_inventaris'));
    //     $this->db->update('inventaris',$data);
    // }
 }

?>
