<?php


class Room_admin_rng extends Ci_model{
    public function getAllrng()
    {
        return $this->db->get('ruang')->result_array();
     
    }
    public function tambahDataRuang()
    {
        $data = [
          
            "id_ruang" => $this->input->post('id_ruang', true),
            "nama_ruang" => $this->input->post('nama_ruang',true),
            "kode_ruang" => $this->input->post('kode_ruang',true),
            "keterangan" => $this->input->post('keterangan',true),
            
        
        ];
        // var_dump ($data); die;

        $this->db->insert('ruang',$data);
    }
    public function hapusDataRuang($where)
    {
      
        $this->db->delete('ruang',$where);
    }
    public function getRuangById($id)
    {
        return $this->db->get_where('ruang',['id_ruang' => $id])->row();
    }
    public function ubahDataRuang()
    {
        $data = [
            
            "id_ruang" => $this->input->post('id_ruang', true),
            "nama_ruang" => $this->input->post('nama_ruang',true),
            "kode_ruang" => $this->input->post('kode_ruang',true),
            "keterangan" => $this->input->post('keterangan',true)
            
        
        ];
        // var_dump ($data); die;
        $this->db->where('id_ruang', $this->input->post('id_ruang'));
        $this->db->update('ruang',$data);
    }
    public function caridataruangan()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nama_ruang', $keyword);
        return $this->db->get('ruang')->result_array();
    }
} 
?>
  