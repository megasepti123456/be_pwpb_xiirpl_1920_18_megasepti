<?php


class Room_pj_pmjm extends Ci_model{
    public function getAllPmjmpj()
    {
        return $this->db->get('peminjaman')->result_array();
     
    }

    public function caridatapeminjaman()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('status_peminjaman', $keyword);
        return $this->db->get('peminjaman')->result_array();
    } 
    public function tambahDataPeminjamanpj() 
    {
        $data = [
            "id_peminjam" => $this->input->post('id_peminjam', true),
            "id_peminjaman" => $this->input->post('id_peminjaman',true),
            "tanggal_pinjam" => $this->input->post('tanggal_pinjam',true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali',true),
            "status_peminjaman" => $this->input->post('status_peminjaman',true)
        
        ];
        

        $this->db->insert('peminjaman',$data);
    }
    
   
    public function getPeminjamanpjById($id)
    {
        return $this->db->get_where('inventaris',['id_inventaris' => $id])->row();
    }
  
} 
?> 
  