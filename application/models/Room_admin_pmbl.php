<?php


class Room_admin_pmbl extends Ci_model{
    public function getAllPmbl()
    {
        return $this->db->get('ppengembalian')->result_array();
     
    }
    public function cariDatapmbl() {
    	$keyword = $this->input->get('keyword', true);
    	$this->db->like('id_pengembalian', $keyword);
    	
    	return $this->db->get('ppengembalian')->result_array(); 
    } 
    public function tambahDataPengembalian()
    {
        $data = [
            "id_pengembalian" => $this->input->post('id_pengembalian', true),
            "nama_peminjam" => $this->input->post('nama_peminjam', true),
            "tanggal_pinjam" => $this->input->post('tanggal_pinjam', true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali', true),
            "nama" => $this->input->post('nama', true)
          
         
        ];
        // var_dump ($data); die;
 
        $this->db->insert('ppengembalian',$data);
    }
    public function hapusDataPengembalian($where)
    {
      
        $this->db->delete('ppengembalian',$where);
    }
    public function getPengembalianById($id)
    {
        return $this->db->get_where('ppengembalian',['id_pengembalian' => $id])->row();  
    }
    public function ubahDataPengembalian()
    {
        $data = [
            "id_pengembalian" => $this->input->post('id_pengembalian', true),
            "nama_peminjam" => $this->input->post('nama_peminjam', true),
            "tanggal_pinjam" => $this->input->post('tanggal_pinjam', true),
            "tanggal_kembali" => $this->input->post('tanggal_kembali', true),
            "nama" => $this->input->post('nama', true)
          
        
        ];
        // var_dump ($data); die;
        $this->db->where('id_pengembalian', $this->input->post('id_pengembalian'));
        $this->db->update('ppengembalian',$data);
    }
}
?>
