<?php 


class Room_admin_inv extends Ci_model{
    public function getAllInv()
    {
        return $this->db->get('inventaris')->result_array();
     
    }
    public function Listb()
    {
        return $this->db->get('inventaris');
     
    }

    public function getRoom_admin_invById($id)
	{
		return $this->db->get_where('inventaris', ['id_inventaris' => $id])->row_array();
	}
    public function cariDataInv() {
    	$keyword = $this->input->get('keyword', true);
    	$this->db->like('nama', $keyword);
    	
    	return $this->db->get('inventaris')->result_array(); 
    }
    public function tambahDataInventaris(){
        $data = [
            "id_inventaris" => $this->input->post('id_inventaris', true),
            "id_jenis" => $this->input->post('id_jenis', true),
            "id_ruang" => $this->input->post('id_ruang', true),
            "nama" => $this->input->post('nama',true),
            "kondisi" => $this->input->post('kondisi',true),
            "keterangan" => $this->input->post('keterangan',true),
            "jumlah" => $this->input->post('jumlah',true),
            "kode_inventaris" => $this->input->post('kode_inventaris',true),
            "gambar" => $_FILES['gambar']['name'],
            
        
        ];
        
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg/jpeg/png';
        $config['file_name'] = $this->input->post('id_inventaris');
        
       // var_dump($_FILES['gambar']['name']);die();
        $gambar = move_uploaded_file($_FILES['gambar']['tmp_name'],'uploads/'.$_FILES['gambar']['name']);

        $this->load->library('upload',$config);


        if( !$gambar ) {
            $error = ['error' => $this->upload->display_errors()]; 
            $this->load->view('admin/tambahinv', $error);

        }else{
           
            if($this->db->insert('inventaris',$data)) {
                redirect('inventarisd/index');
            
            
        }
    }

 }

       
    public function hapusDataInventaris($where)
    {
      
        $this->db->delete('inventaris',$where);
    }
    public function getInventarisById($id)
    {
        return $this->db->get_where('inventaris',['id_inventaris' => $id])->row();
    }
    public function ubahDataInventaris()
    {
       
        $data = [
          
            "id_inventaris" => $this->input->post('id_inventaris', true),
            "id_jenis" => $this->input->post('id_jenis', true),
            "id_ruang" => $this->input->post('id_ruang', true),
            "nama" => $this->input->post('nama',true),
            "gambar" => $_FILES['gambar']['name'],
            "kondisi" => $this->input->post('kondisi',true),
            "keterangan" => $this->input->post('keterangan',true),
            "jumlah" => $this->input->post('jumlah',true),
            "kode_inventaris" => $this->input->post('kode_inventaris',true)
        
        ];
       
        
        // var_dump ($data); die;
        $this->db->where('id_inventaris', $this->input->post('id_inventaris'));
        $this->db->update('inventaris',$data);
    }
 }

?>
