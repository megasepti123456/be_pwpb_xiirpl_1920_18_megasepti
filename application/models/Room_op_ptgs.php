<?php


class Room_op_ptgs extends Ci_model{ 
    public function getAllptgop()
    {
        return $this->db->get('petugas')->result_array();
     
    }
    public function tambahDataPetugasop()
    {
        $data = [
            
            "id_petugas" => $this->input->post('id_petugas', true),
            "username" => $this->input->post('username',true),
            "password" => $this->input->post('password',true)
        
        ];
        //var_dump ($data); die; 

        $this->db->insert('petugas',$data);
    }
    public function hapusDataPetugasop($where)
    {
      
        $this->db->delete('petugas',$where);
    }
    public function getPetugasopById($id)
    {
        return $this->db->get_where('petugas',['id_petugas' => $id])->row();
    }
    public function ubahDataPetugasop()
    {
        $data = [
            "id_petugas" => $this->input->post('id_petugas', true),
            "username" => $this->input->post('username',true),
            "password" => $this->input->post('password',true)
        
        ];
        //var_dump ($data); die;
        $this->db->where('id_petugas', $this->input->post('id_petugas'));
        $this->db->update('petugas',$data);
    }

    public function caridatapetugasop()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('username', $keyword);
        return $this->db->get('petugas')->result_array();
    }
}  
?> 
  