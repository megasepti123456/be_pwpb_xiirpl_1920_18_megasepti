<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Hpadmin extends CI_Controller {
    public function index()
	{
       
		$this->load->view('admin/index');
	}
	function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect('auth');
    }
}