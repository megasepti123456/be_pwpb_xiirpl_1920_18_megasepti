<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pengembaliand extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_admin_pmbl');
	 
	} 
    public function index()
	{
		$data['pmblan']=$this->Room_admin_pmbl->getAllPmbl();
       
		$this->load->view('admin/datapengembalian',$data);
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['pmbl'] = $this->Room_admin_pmbl->getPengembalianById($id);
		$this->load->view('admin/detailpmbl', $data);
			
	}
	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		//$data['pml'] = $this->Room_admin_pmbl->getPengembalianById($id); 
		$this->form_validation->set_rules('id_pengembalian','Id_pengembalian','required',
											['required'=>'id pengembalian harus diisi']);
											$this->form_validation->set_rules('nama_peminjam','nama_peminjam','required',
											['required'=>'nama_peminjam harus diisi']);
											$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
											['required'=>'tanggal pinjam harus diisi']);
											$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
											['required'=>'tanggal kembali harus diisi']);
											$this->form_validation->set_rules('nama','nama','required',
											['required'=>'nama harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/tambahpml',$data);
		
	} else {
		$this->Room_admin_pmbl->tambahDataPengembalian();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('pengembaliand/index');
		}
	
	} 
	


	public function hapus($id)
	{
		$where=array(
			'id_pengembalian'=>$id
		);
		$this->Room_admin_pmbl->hapusDataPengembalian($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('pengembaliand/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['pml'] = $this->Room_admin_pmbl->getPengembalianById($id);
		$this->form_validation->set_rules('id_pengembalian','Id_pengembalian','required',
										['required'=>'id pengembalian harus diisi']);
										$this->form_validation->set_rules('nama_peminjam','nama_peminjam','required',
										['required'=>'nama_peminjam harus diisi']);
										$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
										['required'=>'tanggal pinjam harus diisi']);
										$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
										['required'=>'tanggal kembali harus diisi']);
										$this->form_validation->set_rules('nama','nama','required',
										['required'=>'nama harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/ubahpml',$data);
		
	} else {
		$this->Room_admin_pmbl->ubahDataPengembalian(); 
		$this->session->set_flashdata('flash','Diubah');
		redirect('pengembaliand/index');
		}
	
	} 
}