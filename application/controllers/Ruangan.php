<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ruangan extends CI_Controller {
	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('Room_admin_rng');
	
	}
    public function index()
	{
		$data['ruang']=$this->Room_admin_rng->getAllrng();
		if( $this->input->post('keyword')){
			$data['ruang'] = $this->Room_admin_rng->caridataruangan();
		}
		$this->load->view('admin/dataruangan',$data);
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['rng'] = $this->Room_admin_rng->getRuangById($id);
		$this->load->view('admin/detailrng', $data);
			
	}
	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_ruang','Id_ruang','required',
										['required'=>'id ruang harus diisi']);
										$this->form_validation->set_rules('nama_ruang','nama_ruang','required',
										['required'=>'nama_ruang  harus diisi']);
										$this->form_validation->set_rules('kode_ruang','kode_ruang','required',
										['required'=>'kode_ruang harus diisi']);
										$this->form_validation->set_rules('keterangan','keterangan','required',
										['required'=>'keterangan  harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/tambahrng',$data);
		
	} else {
		$this->Room_admin_rng->tambahDataRuang();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('ruangan/index');
		}
	
	} 
	 


	public function hapus($id)
	{
		$where=array(
			'id_ruang'=>$id
		);
		$this->Room_admin_rng->hapusDataRuang($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('ruangan/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['rng'] = $this->Room_admin_rng->getRuangById($id);
		$this->form_validation->set_rules('id_ruang','Id_ruang','required',
										['required'=>'id ruang harus diisi']);
										$this->form_validation->set_rules('nama_ruang','nama_ruang','required',
										['required'=>'nama ruang harus diisi']);
										$this->form_validation->set_rules('kode_ruang','kode_ruang','required',
										['required'=>'kode_ruang harus diisi']);
										$this->form_validation->set_rules('keterangan','keterangan','required',
										['required'=>'keterangan harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/ubahrng',$data);
		
	} else {
		$this->Room_admin_rng->ubahDataRuang();
		$this->session->set_flashdata('flash','Diubah');
		redirect('ruangan/index');
		}
	
	} 
}