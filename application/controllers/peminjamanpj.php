<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjamanpj extends CI_Controller{
	public function __construct()
{
	parent::__construct();
	$this->load->model('Room_pj_pmjm');
	$this->load->library('form_validation');

}


	
    public function index()
	{
		
		$data['pmjmanpj']=$this->Room_pj_pmjm->getAllPmjmpj();
	// 	 var_dump($data); 
	//   die;
		$this->load->view('peminjam/datapeminjaman',$data);
	
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['invers'] = $this->Room_pj_pmjm->getPeminjamanpjById($id);
		$this->load->view('peminjam/detailbarang', $data);
			
	}
 
	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
										['required'=>'id peminjam harus diisi']);
										$this->form_validation->set_rules('id_peminjaman','Id_peminjaman','required',
										['required'=>'id peminjaman harus diisi']);
										$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
										['required'=>'tanggal pinjam harus diisi']);
										$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
										['required'=>'tanggal kembali harus diisi']);
										$this->form_validation->set_rules('status_peminjaman','status_peminjaman','required',
										['required'=>'status peminjaman harus diisi']);
		if($this->form_validation->run() == FALSE ) {
		$this->load->view('peminjam/tambahdatapmjmpj',$data);
		
	} else {
		$this->Room_pj_pmjm->tambahDataPeminjamanpj();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('peminjamanpj/index');
		}
	
	}  
	

}