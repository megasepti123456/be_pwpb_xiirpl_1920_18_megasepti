<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventarisd extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_admin_inv');
		$this->load->helper(array('url','form','download'));
	
	}
    public function index()
	{
		$data['hasil'] = $this->Room_admin_inv->cariDatainv();
		$data['inventaris']=$this->Room_admin_inv->getAllInv();
		$this->load->view('admin/datainventaris',$data);
	}
	
	
		public function detail($id) {
			$data['judul'] = 'Detail Data ';
			$data['invers'] = $this->Room_admin_inv->getInventarisById($id);
			$this->load->view('admin/detailinv', $data);
				
		}
	public function tambah() 
	{
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_inventaris','Id_inventaris','required',
										['required'=>'id inventaris harus diisi']);
										$this->form_validation->set_rules('id_jenis','id_jenis','required',
										['required'=>'id_jenis harus diisi']);
										$this->form_validation->set_rules('id_ruang','id_ruang','required',
										['required'=>'id_ruang harus diisi']);
										$this->form_validation->set_rules('nama','Nama','required',
										['required'=>'nama harus diisi']);
										$this->form_validation->set_rules('kondisi','Kondisi','required',
										['required'=>' kondisi harus diisi']);
										$this->form_validation->set_rules('keterangan','Keterangan','required',
										['required'=>' keterangan harus diisi']);
										$this->form_validation->set_rules('jumlah','Jumlah','required',
										['required'=>'status jumlah harus diisi']);
										$this->form_validation->set_rules('kode_inventaris','kode_inventaris','required',
										['required'=>' Kode Inventaris harus diisi']);
									
		if($this->form_validation->run() == FALSE ) {
		$this->load->view('admin/tambahinv',$data);
		
	} else {
		$this->Room_admin_inv->tambahDataInventaris();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('inventarisd/index');
		} 
		
	 
	} 
	


	public function hapus($id)
	{
		$where=array(
			'id_inventaris'=>$id
		);
		$this->Room_admin_inv->hapusDataInventaris($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('Inventarisd/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['invers'] = $this->Room_admin_inv->getInventarisById($id);
		$this->form_validation->set_rules('id_inventaris','Id_inventaris','required',
										['required'=>'id inventaris harus diisi']);
										$this->form_validation->set_rules('id_jenis','id_jenis','required',
										['required'=>'id_jenis harus diisi']);
										$this->form_validation->set_rules('id_ruang','id_ruang','required',
										['required'=>'id_ruang harus diisi']);
										$this->form_validation->set_rules('nama','Nama','required',
										['required'=>'nama harus diisi']);
										$this->form_validation->set_rules('kondisi','Kondisi','required',
										['required'=>' kondisi harus diisi']);
										$this->form_validation->set_rules('keterangan','Keterangan','required',
										['required'=>' keterangan harus diisi']);
										$this->form_validation->set_rules('jumlah','Jumlah','required',
										['required'=>'status jumlah harus diisi']);
										$this->form_validation->set_rules('kode_inventaris','kode_inventaris','required',
										['required'=>' Kode Inventaris harus diisi']);
		if($this->form_validation->run() == FALSE ) {
		$this->load->view('admin/ubahinv',$data);
		
	} else {
		$this->Room_admin_inv->ubahDataInventaris();
		$this->session->set_flashdata('flash','Diubah');
		redirect('inventarisd/index');
		}
	 
	} 
	public function cetak() {
		$data['judul'] = 'Cetak Data';
		$data['inventaris'] = $this->Room_admin_inv->getAllInv();
		$this->load->view('admin/cetak', $data);
			
	}
	
} 
