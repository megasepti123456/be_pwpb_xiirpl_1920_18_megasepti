<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Petugas extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_admin_ptgs');
	
	}
    public function index()
	{ 
		$data['ptg']= $this->Room_admin_ptgs->getAllptg();
		if( $this->input->post('keyword')){
			$data['ptg'] = $this->Room_admin_ptgs->caridatapetugas();
	 	}
       
		$this->load->view('admin/datapetugas',$data);
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['ptg'] = $this->Room_admin_ptgs->getPetugasById($id);
		$this->load->view('admin/detailptgs', $data);
			
	}
	public function tambah()
	{ 
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_petugas','Id_petugas','required',
										['required'=>'id petugas harus diisi']);
										$this->form_validation->set_rules('username','username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('password','password','required',
										['required'=>'password kembali harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/tambahptg',$data);
		
	} else {
		$this->Room_admin_ptgs->tambahDataPetugas();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('petugas/index');
		}
	
	}  
	


	public function hapus($id)
	{
		$where=array(
			'id_petugas'=>$id
		);
		$this->Room_admin_ptgs->hapusDataPetugas($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('petugas/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['ptg'] = $this->Room_admin_ptgs->getPetugasById($id);
		$this->form_validation->set_rules('id_petugas','Id_petugas','required',
										['required'=>'id petugas harus diisi']);
										$this->form_validation->set_rules('username','Username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('passsword','Password','required',
                                        ['required'=>'password  harus diisi']);
                                       
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/ubahptg',$data);
		
	} else {
		$this->Room_admin_ptgs->ubahDataPetugas();
		$this->session->set_flashdata('flash','Diubah');
		redirect('petugas/index');
		}
	
    } 
    public function ubahpetugas()
	{
		
		$this->Room_admin_ptgs->ubahDataPetugas();
		$this->session->set_flashdata('flash','Diubah');
		redirect('petugas/index');
	
	
	} 
	
    
}