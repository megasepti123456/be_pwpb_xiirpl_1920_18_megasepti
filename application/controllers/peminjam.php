<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Peminjam extends CI_Controller {
	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('Room_admin_peminjam');
	
	}
    public function index()
	{
		$data['pj']=$this->Room_admin_peminjam->getAllpmjm();
		if( $this->input->post('keyword')){
			$data['pj'] = $this->Room_admin_peminjam->caridatapeminjam();
		}
		$this->load->view('admin/datapeminjam',$data);
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['pj'] = $this->Room_admin_peminjam->getPeminjamById($id);
		$this->load->view('admin/detailpmj', $data);
			
	}
	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
										['required'=>'id peminjam harus diisi']);
										$this->form_validation->set_rules('username','username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('password','password','required',
                                        ['required'=>'password harus diisi']);
                                        $this->form_validation->set_rules('id_level','id_level','required',
										['required'=>'id_level harus diisi']);
										$this->form_validation->set_rules('nama_peminjam','nama_peminjam','required',
                                        ['required'=>'nama_peminjam harus diisi']);
                                        $this->form_validation->set_rules('alamat','alamat','required',
										['required'=>'alamat harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/tambahpmj',$data);
		
	} else {
		$this->Room_admin_peminjam->tambahDataPeminjam();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('peminjam/index');
		}
	
	} 
	


	public function hapus($id)
	{
		$where=array(
			'id_peminjam'=>$id
		);
		$this->Room_admin_peminjam->hapusDataPeminjam($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('peminjam/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['pj'] = $this->Room_admin_peminjam->getPeminjamById($id);
		//var_dump($data['pj']);die();
		$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
										['required'=>'id peminjam harus diisi']);
										$this->form_validation->set_rules('username','username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('password','password','required',
                                        ['required'=>'password harus diisi']);
                                        $this->form_validation->set_rules('id_level','id_level','required',
										['required'=>'id_level harus diisi']);
										$this->form_validation->set_rules('nama_peminjam','nama_peminjam','required',
                                        ['required'=>'nama_peminjam harus diisi']);
                                        $this->form_validation->set_rules('alamat','alamat','required',
										['required'=>'alamat harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('admin/ubahpmj',$data);
		
	} else {
		$this->Room_admin_peminjam->ubahDataPeminjam();
		$this->session->set_flashdata('flash','Diubah');
		redirect('peminjam/index');
		} 
	
	} 
}