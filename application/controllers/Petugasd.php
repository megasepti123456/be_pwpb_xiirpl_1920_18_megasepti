<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Petugasd extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_op_ptgs');
	
	}
    public function index()
	{ 
		$data['ptgop']= $this->Room_op_ptgs->getAllptgop();
		if( $this->input->post('keyword')){
			$data['ptgop'] = $this->Room_op_ptgs->caridatapetugasop();
	 	}
       
		$this->load->view('oprator/datapetugas',$data);
	}
	
	public function tambah()
	{ 
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_petugas','Id_petugas','required',
										['required'=>'id petugas harus diisi']);
										$this->form_validation->set_rules('username','username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('password','password','required',
										['required'=>'password kembali harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('oprator/tambahptgop',$data);
		
	} else {
		$this->Room_op_ptgs->tambahDataPetugasop();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('petugasd/index');
		}
	
	}  
	


	public function hapus($id)
	{
		$where=array(
			'id_petugas'=>$id
		);
		$this->Room_op_ptgs->hapusDataPetugasop($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('petugasd/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['ptgop'] = $this->Room_op_ptgs->getPetugasopById($id);
		$this->form_validation->set_rules('id_petugas','Id_petugas','required',
										['required'=>'id petugas harus diisi']);
										$this->form_validation->set_rules('username','Username','required',
										['required'=>'username harus diisi']);
										$this->form_validation->set_rules('passsword','Password','required',
                                        ['required'=>'password  harus diisi']);
                                       
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('oprator/ubahptgop',$data);
		
	} else {
		$this->Room_op_ptgs->ubahDataPetugasop();
		$this->session->set_flashdata('flash','Diubah');
		redirect('petugasd/index');
		}
	
    } 
    public function ubahpetugasop()
	{
		
		$this->Room_op_ptgs->ubahDataPetugasop();
		$this->session->set_flashdata('flash','Diubah');
		redirect('petugasd/index');
	
	
	} 
	
    
}