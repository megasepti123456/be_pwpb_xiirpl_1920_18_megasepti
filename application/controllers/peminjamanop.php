<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjamanop extends CI_Controller{
	public function __construct()
{
	parent::__construct();
	$this->load->model('Room_op_pmjm');
	$this->load->library('form_validation');

}


	
    public function index()
	{
		
		$data['pmjmanop']=$this->Room_op_pmjm->getAllPmjmop();
	// 	 var_dump($data); 
	//   die;
		$this->load->view('oprator/datapeminjamanop',$data);
	
	}
	

	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
										['required'=>'id peminjam harus diisi']);
										$this->form_validation->set_rules('id_peminjaman','Id_peminjaman','required',
										['required'=>'id peminjaman harus diisi']);
										$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
										['required'=>'tanggal pinjam harus diisi']);
										$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
										['required'=>'tanggal kembali harus diisi']);
										$this->form_validation->set_rules('status_peminjaman','status_peminjaman','required',
										['required'=>'status peminjaman harus diisi']);
		if($this->form_validation->run() == FALSE ) {
		$this->load->view('oprator/tambahpmjmop',$data);
		
	} else {
		$this->Room_op_pmjm->tambahDataPeminjamanop();
		$this->session->set_flashdata('flash','Ditambahkan');
		redirect('peminjamanop/index');
		}
	
	}  
	
 

	public function hapus($id)
	{
		$where=array(
			'id_peminjaman'=>$id 
		);
		$this->Room_op_pmjm->hapusDataPeminjamanop($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('peminjamanop/index');
	}
	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['pmjmanop'] = $this->Room_op_pmjm->getPeminjamanopById($id);
		$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
										['required'=>'id peminjam harus diisi']);
										$this->form_validation->set_rules('id_peminjaman','Id_peminjaman','required',
										['required'=>'id peminjaman harus diisi']);
										$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
										['required'=>'tanggal pinjam harus diisi']);
										$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
										['required'=>'tanggal kembali harus diisi']);
										$this->form_validation->set_rules('status_peminjaman','status_peminjaman','required',
										['required'=>'status peminjaman harus diisi']);
		if($this->form_validation->run() == FALSE ) {
		$this->load->view('oprator/ubahpmjmop',$data);
		
	} else {
		$this->Room_op_pmjm->ubahDataPeminjamanop();
		$this->session->set_flashdata('flash','Diubah');
		redirect('peminjamanop/index');
		}
	
	}  
}