<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjamand extends CI_Controller{
	public function __construct()
{
	parent::__construct();
	$this->load->model('Room_admin_pmjm');
	$this->load->library('form_validation');

}


	
    public function index()
	{
		
		$data['pmjman']=$this->Room_admin_pmjm->getAllPmjm();
	// 	 var_dump($data); 
	//   die;
		$this->load->view('admin/datapeminjaman',$data);
	
	}
	public function detail($id) {
		$data['judul'] = 'Detail Data ';
		$data['pmjman'] = $this->Room_admin_pmjm->getPeminjamanById($id);
		$this->load->view('admin/detailpmjn', $data);
			
	}

	// public function tambah()
	// {
	// 	$data['judul'] = 'form tambah data';
	// 	$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
	// 									['required'=>'id peminjam harus diisi']);
	// 									$this->form_validation->set_rules('id_peminjaman','Id_peminjaman','required',
	// 									['required'=>'id peminjaman harus diisi']);
	// 									$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
	// 									['required'=>'tanggal pinjam harus diisi']);
	// 									$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
	// 									['required'=>'tanggal kembali harus diisi']);
	// 									$this->form_validation->set_rules('status_peminjaman','status_peminjaman','required',
	// 									['required'=>'status peminjaman harus diisi']);
	// 	if($this->form_validation->run() == FALSE ) {
	// 	$this->load->view('admin/tambahpmjm',$data);
		
	// } else {
	// 	$this->Room_admin_pmjm->tambahDataPeminjaman();
	// 	$this->session->set_flashdata('flash','Ditambahkan');
	// 	redirect('peminjamand/index');
	// 	}
	
	// }  
	
 

	// public function hapus($id)
	// {
	// 	$where=array(
	// 		'id_peminjaman'=>$id 
	// 	);
	// 	$this->Room_admin_pmjm->hapusDataPeminjaman($where);
	// 	$this->session->set_flashdata('flash','Dihapus');
	// 	redirect('peminjamand/index');
	// }
	// public function ubah($id)
	// {
	// 	$data['judul'] = 'form ubah data';
	// 	$data['pmjman'] = $this->Room_admin_pmjm->getPeminjamanById($id);
	// 	$this->form_validation->set_rules('id_peminjam','Id_peminjam','required',
	// 									['required'=>'id peminjam harus diisi']);
	// 									$this->form_validation->set_rules('id_peminjaman','Id_peminjaman','required',
	// 									['required'=>'id peminjaman harus diisi']);
	// 									$this->form_validation->set_rules('tanggal_pinjam','tanggal_pinjam','required',
	// 									['required'=>'tanggal pinjam harus diisi']);
	// 									$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
	// 									['required'=>'tanggal kembali harus diisi']);
	// 									$this->form_validation->set_rules('status_peminjaman','status_peminjaman','required',
	// 									['required'=>'status peminjaman harus diisi']);
	// 	if($this->form_validation->run() == FALSE ) {
	// 	$this->load->view('admin/ubahpmjm',$data);
		
	// } else {
	// 	$this->Room_admin_pmjm->ubahDataPeminjaman();
	// 	$this->session->set_flashdata('flash','Diubah');
	// 	redirect('peminjamand/index');
	// 	}  
	
	// }  
}