<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pengembalianop extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_op_pmbl');
	 
	} 
    public function index()
	{
		$data['pmblanop']=$this->Room_op_pmbl->getAllPmblop();
       
		$this->load->view('oprator/datapengembalianop',$data);
	}
	
	public function tambah()
	{
		$data['judul'] = 'form tambah data';
		//$data['pml'] = $this->Room_admin_pmbl->getPengembalianById($id); 
		$this->form_validation->set_rules('id_pengembalian','Id_pengembalian','required',
											['required'=>'id pengembalian harus diisi']);
										
											$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
											['required'=>'tanggal_kembali harus diisi']);
										
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('oprator/tambahpmlop',$data);
		
	} else {
		$this->Room_op_pmbl->tambahDataPengembalianop();
		$this->session->set_flashdata('flash','Ditambahkan'); 
		redirect('pengembalianop/index');
		}
	
	} 
	


	public function hapus($id)
	{
		$where=array(
			'id_pengembalian'=>$id
		);
		$this->Room_op_pmbl->hapusDataPengembalianop($where);
		$this->session->set_flashdata('flash','Dihapus');
		redirect('pengembalianop/index');
	}

	public function ubah($id)
	{
		$data['judul'] = 'form ubah data';
		$data['pmlop'] = $this->Room_op_pmbl->getPengembalianopById($id);
		$this->form_validation->set_rules('id_pengembalian','Id_pengembalian','required',
										['required'=>'id pengembalian harus diisi']);
										$this->form_validation->set_rules('tanggal_kembali','tanggal_kembali','required',
										['required'=>'tanggal kembali harus diisi']);
									
		if($this->form_validation->run() == FALSE ) { 
		$this->load->view('oprator/ubahpmlop',$data);
		
	} else {
		$this->Room_op_pmbl->ubahDataPengembalian(); 
		$this->session->set_flashdata('flash','Diubah');
		redirect('pengembalianop/index');
		}
	
	} 
}