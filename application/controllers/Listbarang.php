<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listbarang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Room_admin_inv');
		$this->load->helper(array('url','form','download'));
	
	}
    public function index()
	{
		$data['inventaris']=$this->Room_admin_inv->getAllInv();
		$this->load->view('peminjam/listbarang',$data);
	
	}
	
}